﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Scores
{
    public class ScoreByTeacherVM
    {
        public Guid StudentCode { get; set; }
        public string StudentName { get; set; }
        public double FifteenMinute { get; set; }
        public double OnePeriod { get; set; }
        public double MiddtermScore { get; set; }
        public double FinalScore { get; set; }
    }
}
