﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Subject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Scores
{
    public class ScoreStudentVM
    {
        public int SubjectId { get; set; }
        public SubjectVM Subject { get; set; }
        public string StudentId { get; set; }
        public string StudentName { get; set; }
        public string SubjectName { get; set; }
        public string SemesterName { get; set; }
        public double FifteenMinutes { get; set; }
        public double OnePeriod { get; set; }
        public double Middterm { get; set; }
        public double FinalExam { get; set; }
        public string Grade => GradeCaculator();
        public double Average => 1.0*(FifteenMinutes + OnePeriod * 2 + Middterm * 2 + FinalExam * 3) / 8;
        public string GradeCaculator()
        {
            if (Average >= 8.5 && Average <= 10)
                return "A";
            else if (Average >= 7 && Average < 8.5)
                return "B";
            else if (Average >= 5.5 && Average < 7)
                return "C";
            else if (Average >= 4 && Average < 5.5)
                return "D";
            else
                return "F";
        }

    }
}
