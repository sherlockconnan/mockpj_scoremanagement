﻿using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Class
{
    public class ClassVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="This field is must required.")]
        [StringLength(50)]
        public string Name { get; set; }

        public Status Status { get; set; }
    }
}
