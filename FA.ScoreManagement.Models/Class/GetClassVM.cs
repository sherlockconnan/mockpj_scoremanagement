﻿using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.ClassAdmin
{
    public class GetClassVM
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Status Status { get; set; }
    }
}
