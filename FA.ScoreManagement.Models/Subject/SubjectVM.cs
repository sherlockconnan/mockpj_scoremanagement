﻿using FA.ScoreManagement.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Subject
{
    public class SubjectVM
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "This field is must required.")]
        [StringLength(50)]
        public string Name { get; set; }
        [Display(Name = "Teacher Assigned: ")]
        public Guid TeacherAssignId { get; set; }
        [Display(Name = "Class Assigned: ")]
        public int ClassAssignedId { get; set; }
        [Display(Name = "Semester Assigned: ")]
        public int SemesterAssignedId { get; set; }
        [Display(Name = "School Year Assigned: ")]
        public int SchoolYearAssignedId { get; set; }

        public Status Status { get; set; }
    }
}
