﻿using FA.ScoreManagement.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Subject
{
    public class GetSubjectVM
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public string ClassAssigned  { get; set; }
        public string TeacherAssigned { get; set; }
        public string SemesterAssigned { get; set; }
        public string SchoolYearAssigned { get; set; }
        public Status Status { get; set; }
    }
}
