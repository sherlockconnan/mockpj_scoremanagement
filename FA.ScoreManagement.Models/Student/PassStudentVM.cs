﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Student
{
    public class PassStudentVM
    {
        public string OldPass { get; set; }

        public string NewPass { get; set; }

        public string ConfirmPass { get; set; }
    }
}
