﻿using FA.ScoreManagement.Models.Class;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Student
{
    public class AdminStudent
    {
        public Guid Id { get; set; }
        [Display(Name = "Full Name: ")]
        [Required(ErrorMessage = "This field is required")]
        public string FullName { get; set; }
        public bool IsPasswordChange { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        [Display(Name = "Date Of Birth: ")]
        [Required(ErrorMessage = "This field is required")]
        public DateTime Dob { get; set; }
        public string DateOfBirth { get; set; }
        [Display(Name = "Sex: ")]
        [Required(ErrorMessage = "This field is required")]
        public bool Sex { get; set; }
        [Display(Name = "Address: ")]
        [Required(ErrorMessage = "This field is required")]
        public string Address { get; set; }
        [Display(Name = "Class: ")]
        [Required(ErrorMessage = "This field is required")]
        public int? ClassId { get; set; }
        public ClassVM Class { get; set; }
    }
}
