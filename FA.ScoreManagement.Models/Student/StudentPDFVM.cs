﻿using FA.ScoreManagement.Models.Scores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Student
{
    public class StudentPDFVM
    {
        public string FullName { get; set; }
        public string Address { get; set; }
        public string DateOfBirth { get; set; }
        public string UserId { get; set; }
        public string ClassName { get; set; }
        public List<ScoreStudentVM> scoreStudentVMs { get; set; }
    }
}
