﻿using FA.ScoreManagement.Models.Scores;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Teacher
{
    public class AdminTeacher
    {
        public Guid Id { get; set; }
        [Display(Name = "Full Name: ")]
        [Required(ErrorMessage = "This field is required")]
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsPasswordChange { get; set; }
        [Display(Name = "Date Of Birth: ")]
        [Required(ErrorMessage = "This field is required")]
        public DateTime Dob { get; set; }
        public string DateOfBirth { get; set; }
        [Display(Name = "Sex: ")]
        [Required(ErrorMessage = "This field is required")]
        public bool Sex { get; set; }
        [Display(Name = "Address: ")]
        [Required(ErrorMessage = "This field is required")]
        public string Address { get; set; }
    }
}
