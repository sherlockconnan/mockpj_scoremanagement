﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Models.Teacher
{
    public class UpdateTeacherInfoVM
    {
        [Required(ErrorMessage = "This field is required.")]
        [StringLength(50)]
        public string FullName { get; set; }

        [Required(ErrorMessage = "this field is required.")]
        public DateTime Dob { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        public bool Sex { get; set; }

        [Required(ErrorMessage = "This field is required.")]
        [StringLength(200)]
        public string Address { get; set; }
    }
}
