﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Common
{
    public static class NameHelper
    {
        public static string GenerateUserName(string FullName)
        {
            FullName = RemoveDiacritics(FullName);
            var tmp = FullName.Split(" ");
            var res=tmp[tmp.Length-1];
            for (int i = 0; i < tmp.Length - 1; i++)
            {
                res = res + tmp[i][0].ToString().ToUpper();
            }
            return res;
        }
        public static string RemoveDiacritics(string str)
        {
            var normalizedString = str.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString)
            {
                var unicodeCategory = CharUnicodeInfo.GetUnicodeCategory(c);
                if (unicodeCategory != UnicodeCategory.NonSpacingMark)
                {
                    stringBuilder.Append(c);
                }
            }

            return stringBuilder.ToString().Normalize(NormalizationForm.FormC);
        }
    }
}
