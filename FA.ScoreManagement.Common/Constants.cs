﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Common
{
    public enum Status
    {
        Active,
        InActive
    }
    public enum ScoreType
    {
        FifteenMinutes,
        OnePeriod,
        Midterm,
        FinalExam
    }
}
