﻿using FA.ScoreManagement.Data.Configuration;
using FA.ScoreManagement.Data.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace FA.ScoreManagement.Data.Context
{
    public class ScoreManageDbContext : IdentityDbContext<AppUser, AppRole, Guid>
    {
        public ScoreManageDbContext(DbContextOptions<ScoreManageDbContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
            string connectionString = @"Server=CUONG\SQLEXPRESS;Database=MockScore;Trusted_Connection=True;";
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer(connectionString);
            }
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            foreach (var entityType in builder.Model.GetEntityTypes())
            {
                var table = entityType.GetTableName();
                if (table.StartsWith("AspNet"))
                {
                    entityType.SetTableName(table.Substring(6));
                }
            };
            builder.ApplyConfigurationsFromAssembly(typeof(AppUserConfiguration).Assembly);
            builder.Seed();
        }

        public DbSet<Class> Classes { get; set; }
        public DbSet<ClassAssign> ClassAssigns { get; set; }
        public DbSet<SchoolYear> SchoolYears { get; set; }
        public DbSet<Score> Scores { get; set; }
        public DbSet<Semester> Semesters { get; set; }
        public DbSet<Subject> Subjects { get; set; }
    }
}