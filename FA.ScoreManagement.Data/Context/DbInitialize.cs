﻿using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;

namespace FA.ScoreManagement.Data.Context
{
    public static class DbInitialize
    {
        public static void Seed(this ModelBuilder model)
        {
            // Seed Semester
            model.Entity<Semester>().HasData(new Semester[]
                {
                        new(){Id=1, Name="First Semester"},
                        new(){Id=2, Name="Second Semester"},
                        new(){Id=3, Name="Summer Semester"}
                });
            // Seed SchoolYear
            model.Entity<SchoolYear>().HasData(new SchoolYear[]
                {
                        new(){Id=1, Name="2021-2022"},
                        new(){Id=2, Name="2020-2021"},
                        new(){Id=3, Name="2019-2020"}
                });
            // Seed Class
            model.Entity<Class>().HasData(new Class[]
                {
                        new(){Id=1, Name="C#",Status = Status.Active},
                        new(){Id=2, Name="Java",Status = Status.Active},
                        new(){Id=3, Name="Python",Status = Status.Active}
                });
            // Seed Subject
            model.Entity<Subject>().HasData(new Subject[]
                {
                        new(){Id=1, Name="OOP",Status = Status.Active},
                        new(){Id=2, Name="SQL",Status = Status.Active},
                        new(){Id=3, Name="ORM",Status = Status.Active}
                });

            // Seed User
            var passwordHasher = new PasswordHasher<AppUser>();
            var KhaNV2Id = Guid.NewGuid();
            var DungDV21Id = Guid.NewGuid();
            var CuongNV93Id = Guid.NewGuid();
            var YenLT8Id = Guid.NewGuid();
            var TeacherHieuId = Guid.NewGuid();
            var TeacherTuId = Guid.NewGuid();
            var SuperAdmin = Guid.NewGuid();
            AppUser[] users = new AppUser[]
            {
                new(){
                    Id = SuperAdmin,
                    UserName = "Admin",
                    Email = "Admin@gmail.com",
                    FullName = "Lê Hoàng Long",
                    Sex = true,
                    Address = "Xuân Trường",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "admin",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new(){
                    Id = KhaNV2Id,
                    UserName = "KhaNV2",
                    Email = "KhaNV2@gmail.com",
                    FullName = "Nguyễn Văn Khá",
                    Sex = false,
                    ClassId = 1,
                    Address = "Nam Định",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "khanv2",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new(){
                    Id = DungDV21Id,
                    UserName = "DungDV21",
                    Email = "DungDV21@gmail.com",
                    FullName = "Đoàn Văn Dũng",
                    Sex = false,
                    ClassId = 1,
                    Address = "Thanh Hoá",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "dungdv21",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new(){
                    Id = CuongNV93Id,
                    UserName = "CuongNV93",
                    Email = "CuongNV93@gmail.com",
                    FullName = "Hứa Văn Cường",
                    Sex = false,
                    ClassId = 1,
                    Address = "Hưng Yên",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "cuongnv93",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new(){
                    Id = YenLT8Id,
                    UserName = "YenLT8",
                    Email = "YenLT8@gmail.com",
                    FullName = "Lê Thị Yến",
                    Sex = true,
                    ClassId = 1,
                    Address = "Hải Dương",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "yenlt8",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new(){
                    Id = TeacherHieuId,
                    UserName = "HieuHT24",
                    Email = "HieuHT24@gmail.com",
                    FullName = "Hoàng Trọng Hiếu",
                    Sex = false,
                    Address = "Hà Nội",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "hieuht24",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
                new(){
                    Id = TeacherTuId,
                    UserName = "TuTB",
                    Email = "TuTB@gmail.com",
                    FullName = "Trịnh Bá Tú",
                    Sex = false,
                    Address = "Hà Nội",
                    IsPasswordChange = false,
                    Dob = DateTime.Now,
                    NormalizedUserName = "tutb",
                    PasswordHash = passwordHasher.HashPassword(null, "Admin123!"),
                    LockoutEnabled = true,
                    EmailConfirmed = true,
                    SecurityStamp = Guid.NewGuid().ToString()
                },
            };
            model.Entity<AppUser>().HasData(users);

            // Seed Role
            var AdminRoleId = Guid.NewGuid();
            var StudentRoleId = Guid.NewGuid();
            var TeacherRoleId = Guid.NewGuid();
            model.Entity<AppRole>().HasData(
               new AppRole() { Id = AdminRoleId, Name = "SuperAdmin", ConcurrencyStamp = "1", NormalizedName = "SuperAdmin".ToLower() },
               new AppRole() { Id = StudentRoleId, Name = "Student", ConcurrencyStamp = "2", NormalizedName = "Student".ToLower() },
               new AppRole() { Id = TeacherRoleId, Name = "Teacher", ConcurrencyStamp = "3", NormalizedName = "Teacher".ToLower() }
               );
            // Seed UserRole
            model.Entity<IdentityUserRole<Guid>>().HasData(new IdentityUserRole<Guid>[]
            {
                new IdentityUserRole<Guid>() { RoleId = AdminRoleId, UserId = SuperAdmin },
                new IdentityUserRole<Guid>() { RoleId = StudentRoleId, UserId = KhaNV2Id },
                new IdentityUserRole<Guid>() { RoleId = StudentRoleId, UserId = DungDV21Id },
                new IdentityUserRole<Guid>() { RoleId = StudentRoleId, UserId = CuongNV93Id },
                new IdentityUserRole<Guid>() { RoleId = StudentRoleId, UserId = YenLT8Id },
                new IdentityUserRole<Guid>() { RoleId = TeacherRoleId, UserId = TeacherHieuId },
                new IdentityUserRole<Guid>() { RoleId = TeacherRoleId, UserId = TeacherTuId },
            });

            // Seed Score
            model.Entity<Score>().HasData(new Score[]
            {
                // Seed KhaNV2
                new(){Id = 1,TestScore = 9,ScoreType= ScoreType.FifteenMinutes,SubjectId = 1,StudentId = KhaNV2Id },
                new(){Id = 2,TestScore = 7,ScoreType= ScoreType.OnePeriod,SubjectId = 1,StudentId = KhaNV2Id },
                new(){Id = 3,TestScore = 6,ScoreType= ScoreType.Midterm,SubjectId = 1,StudentId = KhaNV2Id },
                new(){Id = 4,TestScore = 8,ScoreType= ScoreType.FinalExam,SubjectId = 1,StudentId = KhaNV2Id },

                new(){Id = 5,TestScore = 2,ScoreType= ScoreType.FifteenMinutes,SubjectId = 2,StudentId = KhaNV2Id },
                new(){Id = 6,TestScore = 3,ScoreType= ScoreType.OnePeriod,SubjectId = 2,StudentId = KhaNV2Id },
                new(){Id = 7,TestScore = 4,ScoreType= ScoreType.Midterm,SubjectId = 2,StudentId = KhaNV2Id },
                new(){Id = 8,TestScore = 7,ScoreType= ScoreType.FinalExam,SubjectId = 2,StudentId = KhaNV2Id },

                new(){Id = 9,TestScore = 5,ScoreType= ScoreType.FifteenMinutes,SubjectId = 3,StudentId = KhaNV2Id },
                new(){Id = 10,TestScore = 6,ScoreType= ScoreType.OnePeriod,SubjectId = 3,StudentId = KhaNV2Id },
                new(){Id = 11,TestScore = 7,ScoreType= ScoreType.Midterm,SubjectId = 3,StudentId = KhaNV2Id },
                new(){Id = 12,TestScore = 8,ScoreType= ScoreType.FinalExam,SubjectId = 3,StudentId = KhaNV2Id },

                //Seed DungDV21
                new(){Id = 13,TestScore = 6,ScoreType= ScoreType.FifteenMinutes,SubjectId = 1,StudentId = DungDV21Id },
                new(){Id = 14,TestScore = 7,ScoreType= ScoreType.OnePeriod,SubjectId = 1,StudentId = DungDV21Id },
                new(){Id = 15,TestScore = 8,ScoreType= ScoreType.Midterm,SubjectId = 1,StudentId = DungDV21Id },
                new(){Id = 16,TestScore = 9,ScoreType= ScoreType.FinalExam,SubjectId = 1,StudentId = DungDV21Id },

                new(){Id = 17,TestScore = 5,ScoreType= ScoreType.FifteenMinutes,SubjectId = 2,StudentId = DungDV21Id },
                new(){Id = 18,TestScore = 7,ScoreType= ScoreType.OnePeriod,SubjectId = 2,StudentId = DungDV21Id },
                new(){Id = 19,TestScore = 6,ScoreType= ScoreType.Midterm,SubjectId = 2,StudentId = DungDV21Id },
                new(){Id = 20,TestScore = 9,ScoreType= ScoreType.FinalExam,SubjectId = 2,StudentId = DungDV21Id },

                new(){Id = 21,TestScore = 2,ScoreType= ScoreType.FifteenMinutes,SubjectId = 3,StudentId = DungDV21Id },
                new(){Id = 22,TestScore = 7,ScoreType= ScoreType.OnePeriod,SubjectId = 3,StudentId = DungDV21Id },
                new(){Id = 23,TestScore = 3,ScoreType= ScoreType.Midterm,SubjectId = 3,StudentId = DungDV21Id },
                new(){Id = 24,TestScore = 5,ScoreType= ScoreType.FinalExam,SubjectId = 3,StudentId = DungDV21Id },

                // Seed CuongNV93

                new(){Id = 25,TestScore = 2,ScoreType= ScoreType.FifteenMinutes,SubjectId = 1,StudentId = CuongNV93Id },
                new(){Id = 26,TestScore = 5,ScoreType= ScoreType.OnePeriod,SubjectId = 1,StudentId = CuongNV93Id },
                new(){Id = 27,TestScore = 6,ScoreType= ScoreType.Midterm,SubjectId = 1,StudentId = CuongNV93Id },
                new(){Id = 28,TestScore = 7,ScoreType= ScoreType.FinalExam,SubjectId = 1,StudentId = CuongNV93Id },

                new(){Id = 29,TestScore = 9,ScoreType= ScoreType.FifteenMinutes,SubjectId = 2,StudentId = CuongNV93Id },
                new(){Id = 30,TestScore = 8,ScoreType= ScoreType.OnePeriod,SubjectId = 2,StudentId = CuongNV93Id },
                new(){Id = 31,TestScore = 7,ScoreType= ScoreType.Midterm,SubjectId = 2,StudentId = CuongNV93Id },
                new(){Id = 32,TestScore = 7,ScoreType= ScoreType.FinalExam,SubjectId = 2,StudentId = CuongNV93Id },

                new(){Id = 33,TestScore = 5,ScoreType= ScoreType.FifteenMinutes,SubjectId = 3,StudentId = CuongNV93Id },
                new(){Id = 34,TestScore = 8,ScoreType= ScoreType.OnePeriod,SubjectId = 3,StudentId = CuongNV93Id },
                new(){Id = 35,TestScore = 9,ScoreType= ScoreType.Midterm,SubjectId = 3,StudentId = CuongNV93Id },
                new(){Id = 36,TestScore = 9,ScoreType= ScoreType.FinalExam,SubjectId = 3,StudentId = CuongNV93Id },
                
                // Seed YenLT8

                new(){Id = 37,TestScore = 10,ScoreType= ScoreType.FifteenMinutes,SubjectId = 1,StudentId = YenLT8Id },
                new(){Id = 38,TestScore = 9,ScoreType= ScoreType.OnePeriod,SubjectId = 1,StudentId = YenLT8Id },
                new(){Id = 39,TestScore = 8,ScoreType= ScoreType.Midterm,SubjectId = 1,StudentId = YenLT8Id },
                new(){Id = 40,TestScore = 9,ScoreType= ScoreType.FinalExam,SubjectId = 1,StudentId = YenLT8Id },

                new(){Id = 41,TestScore = 5,ScoreType= ScoreType.FifteenMinutes,SubjectId = 2,StudentId = YenLT8Id },
                new(){Id = 42,TestScore = 6,ScoreType= ScoreType.OnePeriod,SubjectId = 2,StudentId = YenLT8Id },
                new(){Id = 43,TestScore = 7,ScoreType= ScoreType.Midterm,SubjectId = 2,StudentId = YenLT8Id },
                new(){Id = 44,TestScore = 9,ScoreType= ScoreType.FinalExam,SubjectId = 2,StudentId = YenLT8Id },

                new(){Id = 45,TestScore = 9,ScoreType= ScoreType.FifteenMinutes,SubjectId = 3,StudentId = YenLT8Id },
                new(){Id = 46,TestScore = 8,ScoreType= ScoreType.OnePeriod,SubjectId = 3,StudentId = YenLT8Id },
                new(){Id = 47,TestScore = 7,ScoreType= ScoreType.Midterm,SubjectId = 3,StudentId = YenLT8Id },
                new(){Id = 48,TestScore = 10,ScoreType= ScoreType.FinalExam,SubjectId = 3,StudentId = YenLT8Id },
            });
            // Seed Class Assign
            model.Entity<ClassAssign>().HasData(new ClassAssign[]
            {
                new(){ClassId = 1,TeacherId = TeacherTuId,SchoolYearId = 1,SemesterId = 1,SubjectId = 1},
                new(){ClassId = 1,TeacherId = TeacherTuId,SchoolYearId = 1,SemesterId = 1,SubjectId = 2},
                new(){ClassId = 1,TeacherId = TeacherHieuId,SchoolYearId = 1,SemesterId = 1,SubjectId = 3},
            });
        }
    }
}