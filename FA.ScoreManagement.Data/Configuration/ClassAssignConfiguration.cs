﻿using FA.ScoreManagement.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.ScoreManagement.Data.Configuration
{
    internal class ClassAssignConfiguration : IEntityTypeConfiguration<ClassAssign>
    {
        public void Configure(EntityTypeBuilder<ClassAssign> builder)
        {
            builder.HasKey(ca => new
            {
                ca.SubjectId,
                ca.TeacherId,
                ca.SchoolYearId,
                ca.SemesterId,
                ca.ClassId,
            });

            builder.HasOne(ca => ca.Subject)
                .WithMany(s => s.ClassAssigns)
                .HasForeignKey(ca => ca.SubjectId);

            builder.HasOne(ca => ca.Teacher)
                .WithMany(t => t.ClassAssigns)
                .HasForeignKey(ca => ca.TeacherId);

            builder.HasOne(ca => ca.SchoolYear)
                .WithMany(sy => sy.ClassAssigns)
                .HasForeignKey(ca => ca.SchoolYearId);

            builder.HasOne(ca => ca.Semester)
                .WithMany(s => s.ClassAssigns)
                .HasForeignKey(ca => ca.SemesterId);

            builder.HasOne(ca => ca.Class)
                .WithMany(c => c.ClassAssigns)
                .HasForeignKey(ca => ca.ClassId)
                .OnDelete(DeleteBehavior.NoAction);
        }
    }
}