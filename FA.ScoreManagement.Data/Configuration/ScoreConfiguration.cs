﻿using FA.ScoreManagement.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA.ScoreManagement.Data.Configuration
{
    internal class ScoreConfiguration : IEntityTypeConfiguration<Score>
    {
        public void Configure(EntityTypeBuilder<Score> builder)
        {
            builder.HasOne(st => st.Subject)
                .WithMany(su => su.Scores)
                .HasForeignKey(st => st.SubjectId);

            builder.HasOne(st => st.Student)
                .WithMany(sr => sr.Scores)
                .HasForeignKey(st => st.StudentId);
        }
    }
}