﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace FA.ScoreManagement.Data.Entities
{
    public class AppUser : IdentityUser<Guid>
    {
        public string FullName { get; set; }
        public DateTime Dob { get; set; }
        public bool Sex { get; set; }
        public string Address { get; set; }
        public bool IsPasswordChange { get; set; }
        public int? ClassId { get; set; }
        public Class Class { get; set; }
        public IEnumerable<Score> Scores { get; set; }
        public IEnumerable<ClassAssign> ClassAssigns { get; set; }
    }
}