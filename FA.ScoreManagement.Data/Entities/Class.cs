﻿using FA.ScoreManagement.Common;
using System.Collections.Generic;

namespace FA.ScoreManagement.Data.Entities
{

    public class Class
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Status Status { get; set; }

        public IEnumerable<AppUser> Students { get; set; }

        public IEnumerable<ClassAssign> ClassAssigns { get; set; }
    }
}