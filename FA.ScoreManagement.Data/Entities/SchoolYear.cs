﻿using System.Collections.Generic;

namespace FA.ScoreManagement.Data.Entities
{
    public class SchoolYear
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public IEnumerable<ClassAssign> ClassAssigns { get; set; }
    }
}