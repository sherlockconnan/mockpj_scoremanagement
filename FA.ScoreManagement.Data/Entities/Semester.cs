﻿using System.Collections.Generic;

namespace FA.ScoreManagement.Data.Entities
{
    public class Semester
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<ClassAssign> ClassAssigns { get; set; }
    }
}