﻿using FA.ScoreManagement.Common;
using System.Collections.Generic;

namespace FA.ScoreManagement.Data.Entities
{
    public class Subject
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public Status Status { get; set; }

        public IEnumerable<ClassAssign> ClassAssigns { get; set; }

        public IEnumerable<Score> Scores { get; set; }
    }
}