﻿using FA.ScoreManagement.Common;
using System;

namespace FA.ScoreManagement.Data.Entities
{


    public class Score
    {
        public int Id { get; set; }
        public double TestScore { get; set; }
        public ScoreType ScoreType { get; set; }

        public Subject Subject { get; set; }
        public int SubjectId { get; set; }

        public AppUser Student { get; set; }
        public Guid StudentId { get; set; }
    }
}