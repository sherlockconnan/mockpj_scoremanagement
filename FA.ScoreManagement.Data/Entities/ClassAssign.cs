﻿using System;

namespace FA.ScoreManagement.Data.Entities
{
    public class ClassAssign
    {
        public int SubjectId { get; set; }
        public Guid TeacherId { get; set; }
        public int SchoolYearId { get; set; }
        public int SemesterId { get; set; }
        public int ClassId { get; set; }

        public Subject Subject { get; set; }
        public AppUser Teacher { get; set; }
        public SchoolYear SchoolYear { get; set; }
        public Semester Semester { get; set; }
        public Class Class { get; set; }
    }
}