﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace FA.ScoreManagement.MVC.Areas.Student.Controllers
{
    [Area("Student")]
    [Authorize(Roles = "Student")]
    public class SubjectController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;

        public SubjectController(IUnitOfWork unitOfWork, UserManager<AppUser> userManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            return View();
        }
        public IActionResult ClassScoreBySubjectName(string subjectName)
        {
            ViewBag.SubjectName = subjectName;
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.ClassName = _unitOfWork.ClassRepository.GetAll().FirstOrDefault(x => x.Id == user.ClassId).Name;
            return View();
        }
        public IActionResult ClassScoreBySubjectNameDT(string subjectName)
        {
            var user = _userManager.GetUserAsync(User).Result;
            var classScore = _unitOfWork.SubjectRepository.GetClassScoreBySubjectName(subjectName);
            return Json(new { data = classScore });
        }
    }
}
