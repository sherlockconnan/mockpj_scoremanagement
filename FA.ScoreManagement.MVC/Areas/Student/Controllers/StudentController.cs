﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Scores;
using FA.ScoreManagement.Models.Student;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SelectPdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FA.ScoreManagement.MVC.Areas.Student.Controllers
{
    [Area("Student")]
    [Authorize(Roles = "Student")]
    public class StudentController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public StudentController(IUnitOfWork unitOfWork, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.UserFName = user.FullName;
            ViewBag.StudentId = user.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10);

            var scoreStudentVMs = _unitOfWork.SubjectRepository.GetSubjectAndScore(user.Id);
            List<int> lists = new List<int>() { 0, 0, 0, 0, 0 };
            var className = _unitOfWork.ClassRepository.GetAll().FirstOrDefault();
            ViewBag.ClassName = className.Name;
            foreach (var item in scoreStudentVMs)
            {
                if (item.Grade == "A")
                {
                    lists[0]++;
                }
                if (item.Grade == "B")
                {
                    lists[1]++;
                }
                if (item.Grade == "C")
                {
                    lists[2]++;
                }
                if (item.Grade == "D")
                {
                    lists[3]++;
                }
                if (item.Grade == "F")
                {
                    lists[4]++;
                }
            }
            List<DataPoint> dataPoints = new List<DataPoint>();

            dataPoints.Add(new DataPoint("A", lists[0]));
            dataPoints.Add(new DataPoint("B", lists[1]));
            dataPoints.Add(new DataPoint("C", lists[2]));
            dataPoints.Add(new DataPoint("D", lists[3]));
            dataPoints.Add(new DataPoint("F", lists[4]));
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            List<double> scoreAverage = new List<double>();
            foreach (var item in scoreStudentVMs)
            {
                scoreAverage.Add(item.Average);
            }
            ViewBag.AverageScoreFull = _unitOfWork.ScoreRepository.AverageScore(scoreAverage);
            ViewBag.Grade = _unitOfWork.ScoreRepository.GetGrade(ViewBag.AverageScoreFull);
            return View();
            return View();
        }

        public async Task<IActionResult> StudyResult()
        {
            var user = _userManager.GetUserAsync(User).Result;
            var usersIncludeClass = await _userManager.Users.Include(x => x.Class).SingleAsync(x => x.Id == user.Id);
            var studentPdfVM = new StudentPDFVM();
            studentPdfVM.FullName = usersIncludeClass.FullName;
            studentPdfVM.Address = usersIncludeClass.Address;
            studentPdfVM.DateOfBirth = usersIncludeClass.Dob.ToString("dd/MM/yyyy");
            studentPdfVM.UserId = usersIncludeClass.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10);
            studentPdfVM.ClassName = usersIncludeClass.Class.Name;
            studentPdfVM.scoreStudentVMs = _unitOfWork.SubjectRepository.GetSubjectAndScore(user.Id);
            return View(studentPdfVM);
        }

        public IActionResult StudyResultDT()
        {
            var user = _userManager.GetUserAsync(User).Result;
            var scoreStudentVMs = _unitOfWork.SubjectRepository.GetSubjectAndScore(user.Id);
            return Json(new { data = scoreStudentVMs });
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(PassStudentVM model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("Index");
            }
            var user = await _userManager.GetUserAsync(User);
            var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPass, model.NewPass);
            if (!changePasswordResult.Succeeded)
            {
                foreach (var error in changePasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return Redirect("Index");
            }
            user.IsPasswordChange = true;
            _unitOfWork.SaveChange();
            await _signInManager.RefreshSignInAsync(user);

            return RedirectToAction("Login", "Account", new { area = "Identity" });
        }

        [HttpGet]
        public async Task<IActionResult> UpdateStudentInfoAsync()
        {
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.UserFName = user.FullName;
            ViewBag.UserId = user.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10);
            ViewBag.DoB = user.Dob.ToString("dd/MM/yyyy");
            ViewBag.Address = user.Address;
            ViewBag.Sex = user.Sex ? "Female" : "Male";
            var users = await _userManager.Users.Include(x => x.Class).SingleAsync(x => x.Id == user.Id);
            ViewBag.ClassName = users.Class.Name;
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateStudentInfoAsync(UpdateStudentInfoVM student)
        {
            var user = _userManager.GetUserAsync(User).Result;
            if (student.FullName != null)
                user.FullName = student.FullName;
            if (student.Dob != new DateTime())
                user.Dob = student.Dob;
            if (student.Sex != (true || false))
                user.Sex = student.Sex;
            if (student.Address != null)
                user.Address = student.Address;
            await _unitOfWork.SaveChangeAsync();

            return Redirect("Index");
        }

        public IActionResult ResultStat()
        {
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.UserFName = user.FullName;
            ViewBag.StudentId = user.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10);

            var scoreStudentVMs = _unitOfWork.SubjectRepository.GetSubjectAndScore(user.Id);
            List<int> lists = new List<int>() { 0, 0, 0, 0, 0 };
            var className = _unitOfWork.ClassRepository.GetAll().FirstOrDefault();
            ViewBag.ClassName = className.Name;
            foreach (var item in scoreStudentVMs)
            {
                if (item.Grade == "A")
                {
                    lists[0]++;
                }
                if (item.Grade == "B")
                {
                    lists[1]++;
                }
                if (item.Grade == "C")
                {
                    lists[2]++;
                }
                if (item.Grade == "D")
                {
                    lists[3]++;
                }
                if (item.Grade == "F")
                {
                    lists[4]++;
                }
            }
            List<DataPoint> dataPoints = new List<DataPoint>();

            dataPoints.Add(new DataPoint("A", lists[0]));
            dataPoints.Add(new DataPoint("B", lists[1]));
            dataPoints.Add(new DataPoint("C", lists[2]));
            dataPoints.Add(new DataPoint("D", lists[3]));
            dataPoints.Add(new DataPoint("F", lists[4]));
            ViewBag.DataPoints = JsonConvert.SerializeObject(dataPoints);
            List<double> scoreAverage = new List<double>();
            foreach (var item in scoreStudentVMs)
            {
                scoreAverage.Add(item.Average);
            }
            ViewBag.AverageScoreFull = _unitOfWork.ScoreRepository.AverageScore(scoreAverage);
            ViewBag.Grade = _unitOfWork.ScoreRepository.GetGrade(ViewBag.AverageScoreFull);
            return View();
        }
        [HttpPost]
        public  IActionResult PrintPdf(string html)
        {
            HtmlToPdf objhtml = new HtmlToPdf();
            PdfDocument objdoc = objhtml.ConvertHtmlString(html.ToString());
            byte[] pdf = objdoc.Save();
            objdoc.Close();
            return  File(pdf, "application/pdf", "HtmlContent.pdf");
        }
       
    }
}