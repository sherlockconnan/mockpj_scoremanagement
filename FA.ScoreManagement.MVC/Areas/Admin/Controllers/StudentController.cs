﻿using AutoMapper;
using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Student;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FA.ScoreManagement.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "SuperAdmin")]
    public class StudentController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public StudentController(IUnitOfWork unitOfWork, IMapper mapper, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.ClassList = new SelectList(_unitOfWork.ClassRepository.GetAll(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AdminStudent request)
        {
            if (ModelState.IsValid)
            {
                var student = _mapper.Map<AppUser>(request);
                student.Id = Guid.NewGuid();
                student.UserName = NameHelper.GenerateUserName(request.FullName);
                var students = _userManager.GetUsersInRoleAsync("Student").Result;
                var studentsCount = students.Where(x => x.UserName.Contains(student.UserName)).Count();
                student.UserName = studentsCount != 0 ? student.UserName + (studentsCount+1) : student.UserName;
                student.Email = student.UserName + "@gmail.com";
                await _userManager.CreateAsync(student, "Admin123!");
                await _userManager.AddToRoleAsync(student, "Student");
                _unitOfWork.SaveChange();
                return RedirectToAction("Index");
            }
            ViewBag.ClassList = new SelectList(_unitOfWork.ClassRepository.GetAll(), "Id", "Name");
            return View(request);
        }

        public IActionResult Edit(Guid id)
        {
            var user = _userManager.FindByIdAsync(id.ToString()).Result;
            user.Class = _unitOfWork.ClassRepository.GetById(user.ClassId);
            var studentVM = _mapper.Map<AdminStudent>(user);
            ViewBag.ClassList = new SelectList(_unitOfWork.ClassRepository.GetAll(), "Id", "Name", user.Class.Id);
            return View(studentVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AdminStudent respond)
        {
            if (ModelState.IsValid)
            {
                var student = _userManager.FindByIdAsync(respond.Id.ToString()).Result;

                student.FullName = respond.FullName;
                student.Dob = respond.Dob;
                student.Sex = respond.Sex;
                student.Address = respond.Address;
                student.ClassId = respond.ClassId;
                student.Class = _unitOfWork.ClassRepository.GetById(student.ClassId);
                await _userManager.UpdateAsync(student);
                //await _unitOfWork.SaveChangeAsync();
                return RedirectToAction("Index");
            }
            var user = _userManager.FindByIdAsync(respond.Id.ToString()).Result;
            user.Class = _unitOfWork.ClassRepository.GetById(user.ClassId);
            ViewBag.ClassList = new SelectList(_unitOfWork.ClassRepository.GetAll(), "Id", "Name", user.Class.Id);
            return View(respond);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var user = _userManager.FindByIdAsync(id.ToString()).Result;
            if (user is null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            await _userManager.DeleteAsync(user);
            _unitOfWork.SaveChange();
            return Json(new { success = true, message = "Delete Successful" });
        }


        public IActionResult GetAll(string status)
        {
            var students = _userManager.GetUsersInRoleAsync("Student").Result;
            foreach (var item in students)
            {
                if (item.ClassId.HasValue)
                {
                    item.Class = _unitOfWork.ClassRepository.GetById(item.ClassId.Value);
                }
            }
            var studentsVM = _mapper.Map<List<AdminStudent>>(students);
            foreach (var item in studentsVM)
            {
                if (item.IsPasswordChange)
                {
                    item.Password = "******";
                }
                else
                {
                    item.Password = "Admin123!";
                }
                item.DateOfBirth = item.Dob.ToString("dd/MM/yyyy");
            }
            return Json(new { data = studentsVM });
        }

    }
}
