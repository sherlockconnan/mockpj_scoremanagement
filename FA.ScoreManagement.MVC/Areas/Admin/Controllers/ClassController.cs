﻿using AutoMapper;
using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Class;
using FA.ScoreManagement.Models.ClassAdmin;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FA.ScoreManagement.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles ="SuperAdmin")]

    public class ClassController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ClassController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var classes = _unitOfWork.ClassRepository.GetAll();
            var listClass = _mapper.Map<IList<Class>, IList<GetClassVM>>(classes);

            return Json(new { data = listClass });
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(ClassVM createClassVM)
        {
            if (!ModelState.IsValid)
                return View();

            var Class = _mapper.Map<Class>(createClassVM);
            Class.Status = Status.Active;
            _unitOfWork.ClassRepository.Add(Class);
            await _unitOfWork.SaveChangeAsync();

            return RedirectToAction("Index", "Class");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var Class = _unitOfWork.ClassRepository.Find(id);
            var result = _mapper.Map<Class, ClassVM>(Class);


            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ClassVM classVM)
        {
            var Class = _mapper.Map<Class>(classVM);
            _unitOfWork.ClassRepository.Update(Class);
            await _unitOfWork.SaveChangeAsync();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeState(int id)
        {
            var Class = _unitOfWork.ClassRepository.Find(id);
            if (Class.Status == Status.InActive)
            {
                Class.Status = Status.Active;
            }
            else
            {
                Class.Status = Status.InActive;
            }
            _unitOfWork.ClassRepository.Update(Class);
            await _unitOfWork.SaveChangeAsync();

            return RedirectToAction("Index");
        }
    }
}
