﻿using AutoMapper;
using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Teacher;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FA.ScoreManagement.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "SuperAdmin")]
    public class TeacherController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        public TeacherController(IUnitOfWork unitOfWork, IMapper mapper, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Create()
        {
            ViewBag.SubjectList = new SelectList(_unitOfWork.SubjectRepository.GetAll(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AdminTeacher request)
        {
            if (ModelState.IsValid)
            {
                var teacher = _mapper.Map<AppUser>(request);
                teacher.Id = Guid.NewGuid();
                teacher.UserName = NameHelper.GenerateUserName(request.FullName);
                var teachers = _userManager.GetUsersInRoleAsync("Teacher").Result;
                var teachersCount = teachers.Where(x => x.UserName.Contains(teacher.UserName)).Count();
                teacher.UserName = teachersCount != 0 ? teacher.UserName + (teachersCount + 1) : teacher.UserName;
                teacher.Email = teacher.UserName + "@gmail.com";
                await _userManager.CreateAsync(teacher, "Admin123!");
                await _userManager.AddToRoleAsync(teacher, "Teacher");
                _unitOfWork.SaveChange();
                return RedirectToAction("Index");
            }
            ViewBag.SubjectList = new SelectList(_unitOfWork.SubjectRepository.GetAll(), "Id", "Name");
            return View(request);
        }

        public IActionResult Edit(Guid id)
        {
            var user = _userManager.FindByIdAsync(id.ToString()).Result;
            user.Class = _unitOfWork.ClassRepository.GetById(user.ClassId);
            var studentVM = _mapper.Map<AdminTeacher>(user);
            //ViewBag.ClassList = new SelectList(_unitOfWork.ClassRepository.GetAll(), "Id", "Name", user.Class.Id);
            return View(studentVM);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AdminTeacher respond)
        {
            if (ModelState.IsValid)
            {
                var teacher = _userManager.FindByIdAsync(respond.Id.ToString()).Result;

                teacher.FullName = respond.FullName;
                teacher.Dob = respond.Dob;
                teacher.Sex = respond.Sex;
                teacher.Address = respond.Address;
                // student.ClassId = respond.ClassId;
                //teacher.Class = _unitOfWork.ClassRepository.GetById(teacher.ClassId);
                await _userManager.UpdateAsync(teacher);
                //await _unitOfWork.SaveChangeAsync();
                return RedirectToAction("Index");
            }
            var user = _userManager.FindByIdAsync(respond.Id.ToString()).Result;
            user.Class = _unitOfWork.ClassRepository.GetById(user.ClassId);
            //ViewBag.ClassList = new SelectList(_unitOfWork.ClassRepository.GetAll(), "Id", "Name", user.Class.Id);
            return View(respond);
        }

        [HttpDelete]
        public async Task<IActionResult> Delete(Guid id)
        {
            var user = _userManager.FindByIdAsync(id.ToString()).Result;
            if (user is null)
            {
                return Json(new { success = false, message = "Error while deleting" });
            }
            await _userManager.DeleteAsync(user);
            _unitOfWork.SaveChange();
            return Json(new { success = true, message = "Delete Successful" });
        }


        public IActionResult GetAll(string status)
        {
            var teachers = _userManager.GetUsersInRoleAsync("Teacher").Result;
            foreach (var item in teachers)
            {
                if (item.ClassId.HasValue)
                {
                    item.Class = _unitOfWork.ClassRepository.GetById(item.ClassId.Value);
                }
            }
            var teachersVM = _mapper.Map<List<AdminTeacher>>(teachers);
            foreach (var item in teachersVM)
            {
                if (item.IsPasswordChange)
                {
                    item.Password = "******";
                }
                else
                {
                    item.Password = "Admin123!";
                }
                item.DateOfBirth = item.Dob.ToString("dd/MM/yyyy");
            }
            return Json(new { data = teachersVM });
        }
    }
}
