﻿using AutoMapper;
using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Subject;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FA.ScoreManagement.MVC.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "SuperAdmin")]
    public class SubjectController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;

        public SubjectController(IUnitOfWork unitOfWork, IMapper mapper, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult GetAll(string status)
        {
            var subjects = _unitOfWork.SubjectRepository.GetAll();
            var subjectVMs = _mapper.Map<IList<Subject>, IList<GetSubjectVM>>(subjects);
            foreach (var item in subjectVMs)
            {
                var classAssign = _unitOfWork.ClassAssignRepository.GetAll(includeProperties: "Teacher,Subject,Class,Semester,SchoolYear").FirstOrDefault(x => x.SubjectId == item.Id);
                item.TeacherAssigned = classAssign.Teacher.FullName;
                item.ClassAssigned = classAssign.Class.Name;
                item.SemesterAssigned = classAssign.Semester.Name;
                item.SchoolYearAssigned = classAssign.SchoolYear.Name;
            }

            return Json(new { data = subjectVMs });
        }

        [HttpGet]
        public IActionResult Create()
        {
            var teachers = _userManager.GetUsersInRoleAsync("Teacher").Result;
            ViewBag.TeacherAssign = new SelectList(teachers, "Id", "FullName", teachers[0].FullName);
            var classes = _unitOfWork.ClassRepository.GetAll();
            ViewBag.ClassAssign = new SelectList(classes, "Id", "Name", classes[0].Name);
            var semesters = _unitOfWork.SemesterRepository.GetAll();
            ViewBag.SemesterAssign = new SelectList(semesters, "Id", "Name", semesters[0].Name);
            var schoolYears = _unitOfWork.SchoolYearRepository.GetAll();
            ViewBag.SchoolYearAssign = new SelectList(schoolYears, "Id", "Name", schoolYears[0].Name);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(SubjectVM subjectVM)
        {
            if (!ModelState.IsValid)
            {
                var teachers = _userManager.GetUsersInRoleAsync("Teacher").Result;
                ViewBag.TeacherAssign = new SelectList(teachers, "Id", "FullName", teachers[0].FullName);
                var classes = _unitOfWork.ClassRepository.GetAll();
                ViewBag.ClassAssign = new SelectList(classes, "Id", "Name", classes[0].Name);
                var semesters = _unitOfWork.SemesterRepository.GetAll();
                ViewBag.SemesterAssign = new SelectList(semesters, "Id", "Name", semesters[0].Name);
                var schoolYears = _unitOfWork.SchoolYearRepository.GetAll();
                ViewBag.SchoolYearAssign = new SelectList(schoolYears, "Id", "Name", schoolYears[0].Name);
                return View();
            }
            var subject = _mapper.Map<Subject>(subjectVM);
            subject.Status = Status.Active;
            _unitOfWork.SubjectRepository.Add(subject);
            _unitOfWork.SaveChange();
            var classAssign = new ClassAssign();
            classAssign.SubjectId = subject.Id;
            classAssign.TeacherId = subjectVM.TeacherAssignId;
            classAssign.ClassId = subjectVM.ClassAssignedId;
            classAssign.SemesterId = subjectVM.SemesterAssignedId;
            classAssign.SchoolYearId = subjectVM.SchoolYearAssignedId;

            _unitOfWork.ClassAssignRepository.Add(classAssign);
            await _unitOfWork.SaveChangeAsync();

            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            var subject = _unitOfWork.SubjectRepository.Find(id);
            var result = _mapper.Map<Subject, SubjectVM>(subject);
            var classAssign = _unitOfWork.ClassAssignRepository.GetAll().FirstOrDefault(x => x.SubjectId == id);
            result.TeacherAssignId = classAssign.TeacherId;
            result.ClassAssignedId = classAssign.ClassId;
            result.SemesterAssignedId = classAssign.SemesterId;
            result.SchoolYearAssignedId = classAssign.SchoolYearId;

            var teachers = _userManager.GetUsersInRoleAsync("Teacher").Result;
            ViewBag.TeacherAssign = new SelectList(teachers, "Id", "FullName");
            var classes = _unitOfWork.ClassRepository.GetAll();
            ViewBag.ClassAssign = new SelectList(classes, "Id", "Name");
            var semesters = _unitOfWork.SemesterRepository.GetAll();
            ViewBag.SemesterAssign = new SelectList(semesters, "Id", "Name");
            var schoolYears = _unitOfWork.SchoolYearRepository.GetAll();
            ViewBag.SchoolYearAssign = new SelectList(schoolYears, "Id", "Name");
            return View(result);
        }

        [HttpPost]
        public async Task<IActionResult> Edit(SubjectVM subjectVM)
        {
            var subject = _mapper.Map<Subject>(subjectVM);
            _unitOfWork.SubjectRepository.Update(subject);
            var classAssign = _unitOfWork.ClassAssignRepository.GetAll().FirstOrDefault(x => x.SubjectId == subject.Id);
            if (classAssign.TeacherId != subjectVM.TeacherAssignId || classAssign.ClassId != subjectVM.ClassAssignedId || classAssign.SemesterId != subjectVM.SemesterAssignedId || classAssign.SchoolYearId != subjectVM.SchoolYearAssignedId)
            {
                var newClassAssign = new ClassAssign()
                {
                    SubjectId = subjectVM.Id,
                    TeacherId = subjectVM.TeacherAssignId,
                    ClassId = subjectVM.ClassAssignedId,
                    SemesterId = subjectVM.SemesterAssignedId,
                    SchoolYearId = subjectVM.SchoolYearAssignedId
                };
                _unitOfWork.ClassAssignRepository.Add(newClassAssign);
                _unitOfWork.ClassAssignRepository.Delete(classAssign);
                _unitOfWork.SaveChange();
            }
            await _unitOfWork.SaveChangeAsync();

            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> ChangeState(int id)
        {
            var subject = _unitOfWork.SubjectRepository.Find(id);
            if (subject.Status == Status.InActive)
            {
                subject.Status = Status.Active;
            }
            else
            {
                subject.Status = Status.InActive;
            }
            _unitOfWork.SubjectRepository.Update(subject);
            await _unitOfWork.SaveChangeAsync();
            return RedirectToAction("Index");
        }
    }
}