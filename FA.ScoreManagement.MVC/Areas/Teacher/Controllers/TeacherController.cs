﻿using AutoMapper;
using ClosedXML.Excel;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Scores;
using FA.ScoreManagement.Models.Subject;
using FA.ScoreManagement.Models.Teacher;
using FA.ScoreManagement.Repository.UnitOfWork;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FA.ScoreManagement.MVC.Areas.Teacher.Controllers
{
    [Area("Teacher")]
    [Authorize(Roles = "Teacher")]
    public class TeacherController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<AppUser> _userManager;
        private readonly SignInManager<AppUser> _signInManager;
        private readonly IMapper _mapper;

        public TeacherController(IUnitOfWork unitOfWork, UserManager<AppUser> userManager, SignInManager<AppUser> signInManager, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
        }

        public IActionResult Index()
        {
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.UserName = user.FullName;
            return View();
        }
        [HttpGet]
        public IActionResult ImportScore()
        {

            var user = _userManager.GetUserAsync(User).Result;
            var subjectList = _unitOfWork.SubjectRepository.GetSubjectByTeacher(user.Id);
            var subjectListVM = _mapper.Map<List<TeacherSubjectVM>>(subjectList);
            ViewBag.Subjects = new SelectList(subjectListVM, "Id", "Name", subjectListVM[0].Id);
            return View();
        }
        public IActionResult ImportScoreDT(int id)
        {
            var lists = _unitOfWork.SubjectRepository.GetScoreByTeacher(id);
            return Json(new { data = lists });
        }

        [HttpGet]
        public IActionResult ChangePassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ChangePassword(PassTeacherVM model)
        {
            if (!ModelState.IsValid)
            {
                return Redirect("Index");
            }
            var user = await _userManager.GetUserAsync(User);
            var changePasswordResult = await _userManager.ChangePasswordAsync(user, model.OldPass, model.NewPass);
            if (!changePasswordResult.Succeeded)
            {
                foreach (var error in changePasswordResult.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
                return Redirect("Index");
            }
            user.IsPasswordChange = true;
            _unitOfWork.SaveChange();
            await _signInManager.RefreshSignInAsync(user);

            return RedirectToAction("Login", "Account", new { area = "Identity" });
        }

        [HttpGet]
        public async Task<IActionResult> UpdateTeacherInfo()
        {
            var user = _userManager.GetUserAsync(User).Result;
            ViewBag.UserFName = user.FullName;
            ViewBag.UserId = user.Id.ToString().Replace("-", "").ToUpper().Substring(0, 10);
            ViewBag.DoB = user.Dob.ToString("dd/MM/yyyy");
            ViewBag.Address = user.Address;
            ViewBag.Sex = user.Sex ? "Female" : "Male";
            var users = await _userManager.Users.Include(x => x.Class).SingleAsync(x => x.Id == user.Id);
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UpdateTeacherInfo(UpdateTeacherInfoVM teacher)
        {
            var user = _userManager.GetUserAsync(User).Result;
            if (teacher.FullName != null)
                user.FullName = teacher.FullName;
            if (teacher.Dob != new DateTime())
                user.Dob = teacher.Dob;
            if (teacher.Sex != (true || false))
                user.Sex = teacher.Sex;
            if (teacher.Address != null)
                user.Address = teacher.Address;
            await _unitOfWork.SaveChangeAsync();

            return Redirect("Index");
        }
        [HttpPost]
        public IActionResult ExportToExcel(int id)
        {
            DataTable dt = new DataTable("Grid");
            dt.Columns.AddRange(new DataColumn[6] { new DataColumn("StudentCode"),
                                                     new DataColumn("StudentName"),
                                                     new DataColumn("FifteenMinute"),
                                                     new DataColumn("OnePeriod"),
                                                     new DataColumn("MiddtermSocre"),
                                                     new DataColumn("FinalScore")
                                                     });

            var lists = _unitOfWork.SubjectRepository.GetScoreByTeacher(id);
            //// TODO Lay ra danh sach hoc sinh hoc mon nay 
            //if (lists.Count == 0)
            //{

            //}
            foreach (var item in lists)
            {
                dt.Rows.Add(item.StudentCode, item.StudentName, item.FifteenMinute, item.OnePeriod,
                    item.MiddtermScore, item.FinalScore);
            }

            using (XLWorkbook wb = new XLWorkbook()) //Install ClosedXml from Nuget for XLWorkbook  
            {
                wb.Worksheets.Add(dt);
                using (MemoryStream stream = new MemoryStream()) //using System.IO;  
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "ExcelFile.xlsx");
                }
            }
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult BatchUserUpload(IFormFile batchUsers,int id)
        {

            if (ModelState.IsValid)
            {
                if (batchUsers?.Length > 0)
                {
                    var stream = batchUsers.OpenReadStream();
                    List<ScoreByTeacherVM> lists = new List<ScoreByTeacherVM>();
                    try
                    {
                        using (var package = new ExcelPackage(stream))
                        {
                            var worksheet = package.Workbook.Worksheets.First();//package.Workbook.Worksheets[0];
                            var rowCount = worksheet.Dimension.Rows;

                            for (var row = 2; row <= rowCount; row++)
                            {
                                try
                                {

                                    var studentCode = worksheet.Cells[row, 1].Value?.ToString();
                                    var studentName = worksheet.Cells[row, 2].Value?.ToString();
                                    var fifteenMinute = worksheet.Cells[row, 3].Value?.ToString();
                                    var onePeriod = worksheet.Cells[row, 4].Value?.ToString();
                                    var middtermSocre = worksheet.Cells[row, 5].Value?.ToString();
                                    var finalScore = worksheet.Cells[row, 6].Value?.ToString();

                                    var scoreByTeacherVM = new ScoreByTeacherVM()
                                    {
                                        StudentCode = Guid.Parse(studentCode),
                                        StudentName = studentName,
                                        FifteenMinute = Convert.ToDouble(fifteenMinute),
                                        OnePeriod=Convert.ToDouble(onePeriod),
                                        MiddtermScore = Convert.ToDouble(middtermSocre),
                                        FinalScore=Convert.ToDouble(finalScore)
                                    };

                                    lists.Add(scoreByTeacherVM);
                                    
                                }
                                catch (Exception)
                                {
                                    return View();
                                }
                            }

                        }
                        _unitOfWork.ScoreRepository.UpsertScore(id, lists);
                        return Redirect(nameof(ImportScore));

                    }
                    catch (Exception)
                    {
                        return View();
                    }
                }
                else
                {
                    return View();
                }
            }
            else
            {
                return View();
            }    
        }
    }
}