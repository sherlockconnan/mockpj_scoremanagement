﻿using AutoMapper;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Class;
using FA.ScoreManagement.Models.ClassAdmin;
using FA.ScoreManagement.Models.Student;
using FA.ScoreManagement.Models.Subject;
using FA.ScoreManagement.Models.Teacher;

namespace FA.ScoreManagement.MVC.Mapping
{
    public class AutoMappingConfig : Profile
    {
        public AutoMappingConfig()
        {
            CreateMap<AppUser, UpdateStudentInfoVM>().ReverseMap();
            CreateMap<AppUser, UpdateTeacherInfoVM>().ReverseMap();
            CreateMap<AppUser, AdminStudent>().ReverseMap();
            CreateMap<AppUser, AdminTeacher>().ReverseMap();
            CreateMap<Class, GetClassVM>().ReverseMap();
            CreateMap<Class, ClassVM>().ReverseMap();
            CreateMap<Subject, GetSubjectVM>().ReverseMap();
            CreateMap<Subject, SubjectVM>().ReverseMap();
            CreateMap<Subject,TeacherSubjectVM>().ReverseMap();
        }
    }
}