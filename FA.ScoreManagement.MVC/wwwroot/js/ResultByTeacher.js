﻿$(document).ready(function () {
    loadStudyResult("1");
});

function loadStudyResult(status) {
    $('#TableResultBySubjectDT').DataTable({
        "destroy": true,
        "ajax": {
            "url": "/Teacher/Teacher/ImportScoreDT?id=" + status
        },
        "columns": [
            { "data": "studentCode", "width": "5%" },
            { "data": "studentName","width": "5%" },
            { "data": "fifteenMinute", "width": "5%" },
            { "data": "onePeriod", "width": "5%" },
            { "data": "middtermScore", "width": "5%" },
            { "data": "finalScore", "width": "5%" },

        ],
    });
}