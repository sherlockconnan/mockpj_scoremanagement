﻿$(document).ready(function () {
    var status = window.location.href.split('=')[1];

    loadStudyResult(status);
});

function loadStudyResult(status) {
    $('#studyClassResult').DataTable({
        "ajax": {
            "url": "/Student/Subject/ClassScoreBySubjectNameDT?subjectName=" + status
        },
        "columns": [
            { "data": "studentId","width":"5%"},
            { "data": "studentName","width":"5%"},
            { "data": "fifteenMinutes","width":"5%"},
            { "data": "onePeriod","width":"5%"},
            { "data": "middterm","width":"5%"},
            { "data": "finalExam","width":"5%"},
            { "data": "average","width":"5%"},
            { "data": "grade","width":"5%"}   
        ],
    });
}