﻿var dataTable;
$(document).ready(function () {
    loadClass("");
});
function loadClass(status) {
    dataTable = $('#ClassTB').DataTable({
        "ajax": {
            "url": "/Admin/Subject/GetAll?status=" + status
        },
        "columns": [
            { "data": "id", "width": "15%" },
            { "data": "name", "width": "15%" },
            { "data": "classAssigned", "width": "10%" },
            { "data": "teacherAssigned", "width": "10%" },
            { "data": "semesterAssigned", "width": "10%" },
            { "data": "schoolYearAssigned", "width": "10%" },
            {
                "data": "status",
                "render": function (data) {
                    var status;
                    if (data == 0) {
                        status = "Active";
                    } else {
                        status = "InActive"
                    }
                    return `
                          ${status}
                            `
                }
                , "width": "15%"
            },
            {
                "data": "id",
                "render": function (data) {
                    return `
                            <div class="w-75 btn-group" role="group">
                            <a href="/Admin/Subject/Edit?id=${data}"
                            class="btn btn-warning mx-2 text-white"> <i class="bi bi-pencil-square"></i> Edit</a>
                           
                            </div>                           
                            `
                },
                "width": "30%"
            }
        ]
    });
}

function Delete(url) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (data) {
                    if (data.success) {
                        dataTable.ajax.reload();
                        toastr.success(data.message);
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            })
        }
    })
}