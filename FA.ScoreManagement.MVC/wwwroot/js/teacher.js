﻿var dataTable;
$(document).ready(function () {
    loadClass("");
});
function loadClass(status) {
    dataTable = $('.TeacherTB').DataTable({
        "ajax": {
            "url": "/Admin/Teacher/GetAll?status=" + status
        },
        "columns": [
            { "data": "fullName", "width": "15%" },
            { "data": "userName", "width": "15%" },
            { "data": "password", "width": "15%" },
            { "data": "dateOfBirth", "width": "15%" },
            {
                "data": "sex",
                "render": function (data) {
                    var sex;
                    if (data) {
                        sex = "Female";
                    } else {
                        sex = "Male"
                    }
                    return `
                          ${sex}
                            `
                }
                , "width": "10%"
            },
            { "data": "address", "width": "15%" },
            {
                "data": "id",
                "render": function (data) {
                    return `
                            <div class="w-75 btn-group" role="group">
                            <a href="/Admin/Teacher/Edit?id=${data}"
                            class="btn btn-warning mx-2 text-white"> <i class="bi bi-pencil-square"></i> Edit</a>
                            <a onClick=Delete('/Admin/Teacher/Delete?id=${data}')
                            class="btn btn-danger mx-2 text-white"> <i class="bi bi-trash-fill"></i> Delete</a>
                            </div>
                            `
                },
                "width": "25%"
            }
        ]
    });
}

function Delete(url) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            $.ajax({
                url: url,
                type: 'DELETE',
                success: function (data) {
                    if (data.success) {
                        dataTable.ajax.reload();
                        toastr.success(data.message);
                    }
                    else {
                        toastr.error(data.message);
                    }
                }
            })
        }
    })
}