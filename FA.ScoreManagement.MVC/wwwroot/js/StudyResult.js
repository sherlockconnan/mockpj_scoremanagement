﻿$(document).ready(function () {
    loadStudyResult("");
    });
function loadStudyResult(status) {
    $('#studyResult').DataTable({
        "ajax": {
            "url": "/Student/Student/StudyResultDT"
        },
        "columns": [
            { "data": "subjectId","width":"5%"},
            {
                "data": "subjectName",
                "render": function (data) {
                    return `
                              <a href="/Student/Subject/ClassScoreBySubjectName?subjectName=${data}">${data}</a>
                            `
                },
                "width": "5%"
            },
            { "data": "semesterName","width":"5%"},
            { "data": "fifteenMinutes","width":"5%"},
            { "data": "onePeriod","width":"5%"},
            { "data": "middterm","width":"5%"},
            { "data": "finalExam","width":"5%"},
            { "data": "average","width":"5%"},
            { "data": "grade","width":"5%"}   
        ],
    });
}