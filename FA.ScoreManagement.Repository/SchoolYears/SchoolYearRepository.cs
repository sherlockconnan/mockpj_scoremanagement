﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.Base;

namespace FA.ScoreManagement.Repository.SchoolYears
{
    internal class SchoolYearRepository : BaseRepository<SchoolYear>, ISchoolYearRepository
    {
        private readonly ScoreManageDbContext context;

        public SchoolYearRepository(ScoreManageDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}