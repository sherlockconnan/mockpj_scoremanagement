﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.Base;

namespace FA.ScoreManagement.Repository.SchoolYears
{
    public interface ISchoolYearRepository : IBaseRepository<SchoolYear>
    {
    }
}