﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Scores;
using FA.ScoreManagement.Repository.Base;
using System;
using System.Collections.Generic;

namespace FA.ScoreManagement.Repository.Scores
{
    public interface IScoreRepository : IBaseRepository<Score>
    {
        double AverageScore(List<double> list);
        string GetGrade(double average);

        void UpsertScore(int subjectId, List<ScoreByTeacherVM> lists);
        
    }
}