﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Scores;
using FA.ScoreManagement.Repository.Base;
using FA.ScoreManagement.Repository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
namespace FA.ScoreManagement.Repository.Scores
{
    public class ScoreRepository : BaseRepository<Score>, IScoreRepository
    {
        private readonly ScoreManageDbContext context;
        public ScoreRepository(ScoreManageDbContext context) : base(context)
        {
            this.context = context;
        }
		public double AverageScore(List<double> list)
		{
            double sum = 0;
			foreach (var item in list)
			{
                sum=sum+item;
			}                
            return 1.0*sum/list.Count;
        }

		public string GetGrade(double average)
		{
            if (average > 8.5)
                return "Very Good";
            else if (average > 7)
                return "Good";
            else if (average > 5.5)
                return "Fair";
            else if (average > 4)
                return "Poor";
            else
                return "Criteria";

        }

        public void UpsertScore(int subjectId, List<ScoreByTeacherVM> lists)
        {
            foreach (var item in lists)
            {
                if (item.FifteenMinute < 0 || item.FifteenMinute > 10)
                    throw new Exception();
                if (item.OnePeriod < 0 || item.OnePeriod > 10)
                    throw new Exception();
                if (item.MiddtermScore < 0 || item.MiddtermScore > 10)
                    throw new Exception();
                if (item.FinalScore < 0 ||   item.FinalScore > 10)
                    throw new Exception();
                var queryFifteen = this.context.Scores.FirstOrDefault(s => s.SubjectId == subjectId && s.StudentId == item.StudentCode && s.ScoreType == Common.ScoreType.FifteenMinutes);
                if(queryFifteen==null)
                {
                    Score score = new Score();
                    score.StudentId = item.StudentCode;
                    score.ScoreType = Common.ScoreType.FifteenMinutes;
                    score.SubjectId=subjectId;
                    score.TestScore = item.FifteenMinute;
                    context.Scores.Add(score);
                    context.SaveChanges();
                }    
                else
                {
                    queryFifteen.TestScore = item.FifteenMinute;
                    context.Scores.Update(queryFifteen);
                    context.SaveChanges();

                }

                var queryOnePeriod = this.context.Scores.FirstOrDefault(s => s.SubjectId == subjectId && s.StudentId == item.StudentCode && s.ScoreType == Common.ScoreType.OnePeriod);
                if (queryOnePeriod == null)
                {
                    Score score = new Score();
                    score.StudentId = item.StudentCode;
                    score.ScoreType = Common.ScoreType.OnePeriod;
                    score.SubjectId = subjectId;
                    score.TestScore = item.OnePeriod;
                    context.Scores.Add(score);
                    context.SaveChanges();

                }
                else
                {
                    queryOnePeriod.TestScore = item.OnePeriod;
                    context.Scores.Update(queryOnePeriod);
                    context.SaveChanges();

                }

                var queryMiddle = this.context.Scores.FirstOrDefault(s => s.SubjectId == subjectId && s.StudentId == item.StudentCode && s.ScoreType == Common.ScoreType.Midterm);
                if (queryMiddle == null)
                {
                    Score score = new Score();
                    score.StudentId = item.StudentCode;
                    score.ScoreType = Common.ScoreType.Midterm;
                    score.SubjectId = subjectId;
                    score.TestScore = item.MiddtermScore;
                    context.Scores.Add(score);
                    context.SaveChanges();

                }
                else
                {
                    queryMiddle.TestScore = item.MiddtermScore;
                    context.Scores.Update(queryMiddle);
                    context.SaveChanges();

                }

                var queryFinal = this.context.Scores.FirstOrDefault(s => s.SubjectId == subjectId && s.StudentId == item.StudentCode && s.ScoreType == Common.ScoreType.FinalExam);
                if (queryFinal == null)
                {
                    Score score = new Score();
                    score.StudentId = item.StudentCode;
                    score.ScoreType = Common.ScoreType.FinalExam;
                    score.SubjectId = subjectId;
                    score.TestScore = item.FinalScore;
                    context.Scores.Add(score);
                    context.SaveChanges();

                }
                else
                {
                    queryFinal.TestScore = item.FinalScore;
                    context.Scores.Update(queryFinal);
                    context.SaveChanges();

                }
            }
        }
    }
}