﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.Base;

namespace FA.ScoreManagement.Repository.Semesters
{
    public interface ISemesterRepository : IBaseRepository<Semester>
    {
    }
}