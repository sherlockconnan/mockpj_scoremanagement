﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.Base;

namespace FA.ScoreManagement.Repository.Semesters
{
    public class SemeterRepository : BaseRepository<Semester>, ISemesterRepository
    {
        private readonly ScoreManageDbContext context;

        public SemeterRepository(ScoreManageDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}