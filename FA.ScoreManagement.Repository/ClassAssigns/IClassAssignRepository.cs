﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.Base;

namespace FA.ScoreManagement.Repository.ClassAssigns
{
    public interface IClassAssignRepository : IBaseRepository<ClassAssign>
    {
    }
}