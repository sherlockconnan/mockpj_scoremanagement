﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Repository.Base;

namespace FA.ScoreManagement.Repository.ClassAssigns
{
    public class ClassAssignRepository : BaseRepository<ClassAssign>, IClassAssignRepository
    {
        private readonly ScoreManageDbContext context;

        public ClassAssignRepository(ScoreManageDbContext context) : base(context)
        {
            this.context = context;
        }
    }
}