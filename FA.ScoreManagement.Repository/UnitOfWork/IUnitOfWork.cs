﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Repository.ClassAssigns;
using FA.ScoreManagement.Repository.Classes;
using FA.ScoreManagement.Repository.SchoolYears;
using FA.ScoreManagement.Repository.Scores;
using FA.ScoreManagement.Repository.Semesters;
using FA.ScoreManagement.Repository.Subjects;
using System;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Repository.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IClassAssignRepository ClassAssignRepository { get; }
        IClassRepository ClassRepository { get; }
        ISchoolYearRepository SchoolYearRepository { get; }
        IScoreRepository ScoreRepository { get; }

        ISemesterRepository SemesterRepository { get; }
        ISubjectRepository SubjectRepository { get; }
        ScoreManageDbContext ScoreManageDbContext { get; }

        int SaveChange();

        Task<int> SaveChangeAsync();
    }
}