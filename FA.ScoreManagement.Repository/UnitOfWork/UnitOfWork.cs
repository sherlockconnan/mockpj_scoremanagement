﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Repository.ClassAssigns;
using FA.ScoreManagement.Repository.Classes;
using FA.ScoreManagement.Repository.SchoolYears;
using FA.ScoreManagement.Repository.Scores;
using FA.ScoreManagement.Repository.Semesters;
using FA.ScoreManagement.Repository.Subjects;
using System.Threading.Tasks;

namespace FA.ScoreManagement.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private IClassAssignRepository _classAssignRepository;
        private IClassRepository _classRepository;
        private ISchoolYearRepository _schoolYearRepository;
        private IScoreRepository _scoreRepository;
        private ISemesterRepository _semesterRepository;
        private ISubjectRepository _subjectRepository;
        private readonly ScoreManageDbContext _scoreManageDbContext;

        public UnitOfWork(ScoreManageDbContext scoreManageDbContext)
        {
            _scoreManageDbContext = scoreManageDbContext;
        }

        public IClassAssignRepository ClassAssignRepository
        {
            get
            {
                if (this._classAssignRepository == null)
                {
                    _classAssignRepository = new ClassAssignRepository(_scoreManageDbContext);
                }
                return _classAssignRepository;
            }
        }

        public IClassRepository ClassRepository
        {
            get
            {
                if (this._classRepository == null)
                {
                    _classRepository = new ClassRepository(_scoreManageDbContext);
                }
                return _classRepository;
            }
        }

        public ISchoolYearRepository SchoolYearRepository
        {
            get
            {
                if (this._schoolYearRepository == null)
                {
                    _schoolYearRepository = new SchoolYearRepository(_scoreManageDbContext);
                }
                return _schoolYearRepository;
            }
        }

        public IScoreRepository ScoreRepository
        {
            get
            {
                if (this._scoreRepository == null)
                {
                    _scoreRepository = new ScoreRepository(_scoreManageDbContext);
                }
                return _scoreRepository;
            }
        }

        public ISemesterRepository SemesterRepository
        {
            get
            {
                if (this._semesterRepository == null)
                {
                    _semesterRepository = new SemeterRepository(_scoreManageDbContext);
                }
                return _semesterRepository;
            }
        }

        public ISubjectRepository SubjectRepository
        {
            get
            {
                if (this._subjectRepository == null)
                {
                    _subjectRepository = new SubjectRepository(_scoreManageDbContext);
                }
                return _subjectRepository;
            }
        }

        public ScoreManageDbContext ScoreManageDbContext => _scoreManageDbContext;

        public void Dispose()
        {
            _scoreManageDbContext.Dispose();
        }

        public int SaveChange()
        {
            return _scoreManageDbContext.SaveChanges();
        }

        public async Task<int> SaveChangeAsync()
        {
            return await _scoreManageDbContext.SaveChangesAsync();
        }
    }
}