﻿using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Class;
using FA.ScoreManagement.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FA.ScoreManagement.Repository.Classes
{
    public class ClassRepository : BaseRepository<Class>, IClassRepository
    {
        private readonly ScoreManageDbContext context;

        public ClassRepository(ScoreManageDbContext context) : base(context)
        {
            this.context = context;
        }


    }
}