﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Class;
using FA.ScoreManagement.Repository.Base;
using System;
using System.Collections.Generic;

namespace FA.ScoreManagement.Repository.Classes
{
    public interface IClassRepository : IBaseRepository<Class>
    {
        
    }
}