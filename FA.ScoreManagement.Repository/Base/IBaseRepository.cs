﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace FA.ScoreManagement.Repository.Base
{
    public interface IBaseRepository<TEntity> where TEntity : class
    {
        TEntity Find(params object[] primarykey);

        void Add(TEntity entity);

        void Update(TEntity entity);

        void Delete(TEntity entity);

        void Delete(params object[] primarykey);

        IList<TEntity> GetAll();

        IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>>? filter = null, string? includeProperties = null);

        TEntity GetById(params object[] primarykey);

        IEnumerable<TEntity> Find(Func<TEntity, bool> condition);
    }
}