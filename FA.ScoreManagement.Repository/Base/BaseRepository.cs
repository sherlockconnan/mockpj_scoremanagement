﻿using FA.ScoreManagement.Data.Context;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FA.ScoreManagement.Repository.Base
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class
    {
        protected readonly ScoreManageDbContext Context;

        protected DbSet<TEntity> DbSet;

        public BaseRepository(ScoreManageDbContext context)
        {
            Context = context;
            DbSet = context.Set<TEntity>();
        }

        public void Add(TEntity entity)
        {
            DbSet.Add(entity);
        }

        public void Delete(TEntity entity)
        {
            DbSet.Remove(entity);
        }

        public void Delete(params object[] primarykey)
        {
            var entity = DbSet.Find(primarykey);
            DbSet.Remove(entity);
        }

        public TEntity Find(params object[] primarykey)
        {
            return DbSet.Find(primarykey);
        }

        public IList<TEntity> GetAll()
        {
            return DbSet.ToList();
        }

        public void Update(TEntity entity)
        {
            Context.Entry(entity).State = EntityState.Modified;
        }

        public virtual IEnumerable<TEntity> GetAll(Expression<Func<TEntity, bool>>? filter = null, string? includeProperties = null)
        {
            IQueryable<TEntity> query = DbSet;
            if (filter != null)
            {
                query = query.Where(filter);
            }
            if (includeProperties != null)
            {
                foreach (var includeProp in includeProperties.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
                {
                    query = query.Include(includeProp);
                }
            }
            return query.ToList();
        }

        public virtual TEntity GetById(params object[] primarykey)
        {
            return this.DbSet.Find(primarykey);
        }

        public virtual IEnumerable<TEntity> Find(Func<TEntity, bool> condition)
        {
            return this.DbSet.Where(condition);
        }
    }
}