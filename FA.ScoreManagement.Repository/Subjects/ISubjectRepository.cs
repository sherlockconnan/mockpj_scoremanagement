﻿using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Scores;
using FA.ScoreManagement.Models.Subject;
using FA.ScoreManagement.Repository.Base;
using System;
using System.Collections.Generic;

namespace FA.ScoreManagement.Repository.Subjects
{
    public interface ISubjectRepository : IBaseRepository<Subject>
    {
        List<ScoreStudentVM> GetSubjectAndScore(Guid studentId);
        List<ScoreStudentVM> GetClassScoreBySubjectName(string subjectName);
        double GetFifteenMinutesScore(Guid studentId, int subjectId);
        double GetOnePeriod(Guid studentId, int subjectId);
        double GetMidTermScore(Guid studentId, int subjectId);
        double GetFinalScore(Guid studentId, int subjectId);
        string GetSemesters(int subjectId);
        List<Subject> GetSubjectByTeacher(Guid id);
        List<ScoreByTeacherVM> GetScoreByTeacher(int subjectId);

    }
}