﻿using FA.ScoreManagement.Common;
using FA.ScoreManagement.Data.Context;
using FA.ScoreManagement.Data.Entities;
using FA.ScoreManagement.Models.Scores;
using FA.ScoreManagement.Models.Subject;
using FA.ScoreManagement.Repository.Base;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FA.ScoreManagement.Repository.Subjects
{
    public class SubjectRepository : BaseRepository<Subject>, ISubjectRepository
    {
        private readonly ScoreManageDbContext _context;

        public SubjectRepository(ScoreManageDbContext context) : base(context)
        {
            _context = context;
        }


        public double GetFifteenMinutesScore(Guid studentId, int subjectId)
        {
            var test = _context.Scores.Where(s => s.StudentId == studentId && s.SubjectId == subjectId && s.ScoreType == ScoreType.FifteenMinutes);

            if (!test.Any())
            {
                return 0;
            }
            else return test.Average(x => x.TestScore);
        }

        public double GetFinalScore(Guid studentId, int subjectId)
        {
            var test = _context.Scores.Where(s => s.StudentId == studentId && s.SubjectId == subjectId && s.ScoreType == ScoreType.FinalExam);
            if (!test.Any())
            {
                return 0;
            }
            else return test.Average(x => x.TestScore);
        }

        public double GetMidTermScore(Guid studentId, int subjectId)
        {
            var test = _context.Scores.Where(s => s.StudentId == studentId && s.SubjectId == subjectId && s.ScoreType == ScoreType.Midterm);
            if (!test.Any())
            {
                return 0;
            }
            else return test.Average(x => x.TestScore);
        }

        public double GetOnePeriod(Guid studentId, int subjectId)
        {
            var test = _context.Scores.Where(s => s.StudentId == studentId && s.SubjectId == subjectId && s.ScoreType == ScoreType.OnePeriod);
            if (!test.Any())
            {
                return 0;
            }
            else return test.Average(x => x.TestScore);
        }

        public string GetSemesters(int subjectId)
        {

            return _context.ClassAssigns.Include(x => x.Semester).Where(x => x.SubjectId == subjectId).Select(x => x.Semester.Name).FirstOrDefault();
        }

        public List<ScoreStudentVM> GetSubjectAndScore(Guid studentId)
        {
            List<ScoreStudentVM> scoreStudentVMs = new List<ScoreStudentVM>();

            var subjectId = _context.Scores.Where(x =>x.StudentId==studentId).Include(x=>x.Subject).Select(x=>x.SubjectId).Distinct();

            foreach (var item in subjectId)
            {
                ScoreStudentVM scoreStudentVM = new ScoreStudentVM();
                scoreStudentVM.SubjectId = item;
                scoreStudentVM.SubjectName = _context.Subjects.FirstOrDefault(s=>s.Id== item).Name;
                scoreStudentVM.SemesterName = GetSemesters(item);
                scoreStudentVM.FifteenMinutes = GetFifteenMinutesScore(studentId, item);
                scoreStudentVM.OnePeriod = GetOnePeriod(studentId, item);
                scoreStudentVM.Middterm = GetMidTermScore(studentId, item);
                scoreStudentVM.FinalExam = GetFinalScore(studentId, item);
                scoreStudentVMs.Add(scoreStudentVM);
            }
            return scoreStudentVMs;
        }

        public List<ScoreStudentVM> GetClassScoreBySubjectName(string subjectName)
        {
            List<ScoreStudentVM> classScore = new List<ScoreStudentVM>();
            var subjectId = _context.Subjects.FirstOrDefault(x => x.Name.Equals(subjectName)).Id;
            var scores = _context.Scores.Include(x => x.Student).GroupBy(x =>new { x.StudentId,x.Student.FullName }).Select(x => new
            {
                Student = x.Key
            }); 
            foreach (var item in scores)
            {
                ScoreStudentVM scoreStudentVM = new ScoreStudentVM();
                var studentId = item.Student.StudentId;
                scoreStudentVM.StudentId = studentId.ToString().Replace("-", "").ToUpper().Substring(0, 10);
                scoreStudentVM.StudentName = item.Student.FullName;
                scoreStudentVM.FifteenMinutes = GetFifteenMinutesScore(studentId, subjectId);
                scoreStudentVM.OnePeriod = GetOnePeriod(studentId, subjectId);
                scoreStudentVM.Middterm = GetMidTermScore(studentId, subjectId);
                scoreStudentVM.FinalExam = GetFinalScore(studentId, subjectId);

                classScore.Add(scoreStudentVM);
            }
            return classScore;
        }

        public List<Subject> GetSubjectByTeacher(Guid id)
        {
            var subject = _context.ClassAssigns.Where(ca => ca.TeacherId == id).Include(ca => ca.Subject).Select(x => x.Subject);

            return subject.ToList();
        }

        public List<ScoreByTeacherVM> GetScoreByTeacher(int subjectId)
        {
            List<ScoreByTeacherVM> scoreByTeacherVMs = new List<ScoreByTeacherVM>();
            var query = (from s in _context.Subjects
                         join sc in _context.Scores
                         on s.Id equals sc.SubjectId
                         join u in _context.Users
                         on sc.StudentId equals u.Id
                         where s.Id == subjectId
                         select new
                         {
                             StudentCode= u.Id,
                             StudentName = u.FullName,
                         }).ToList();
            foreach (var item in query.Distinct())
            {
                ScoreByTeacherVM scoreByTeacherVM=new ScoreByTeacherVM();
                scoreByTeacherVM.StudentCode = item.StudentCode;
                scoreByTeacherVM.StudentName = item.StudentName;
                scoreByTeacherVM.FifteenMinute = GetFifteenMinutesScore(item.StudentCode, subjectId);
                scoreByTeacherVM.FinalScore = GetFinalScore(item.StudentCode, subjectId);
                scoreByTeacherVM.OnePeriod = GetOnePeriod(item.StudentCode, subjectId);
                scoreByTeacherVM.MiddtermScore = GetMidTermScore(item.StudentCode, subjectId);
                scoreByTeacherVMs.Add(scoreByTeacherVM);
            }
            return scoreByTeacherVMs;
        }
    }
}